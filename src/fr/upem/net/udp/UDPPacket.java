package fr.upem.net.udp;

/**
 * Created with IntelliJ IDEA.
 * User: ebabalac
 * Date: 29/01/14
 * Time: 09:58
 * To change this template use File | Settings | File Templates.
 */
public class UDPPacket<T> {
    private final long id;
    private final T data;

    public UDPPacket(long id, T data) {
        this.id = id;
        this.data = data;
    }

    public long getId() {
        return id;
    }

    public T getData() {
        return data;
    }

    public byte[] convertToByteArray(){
        byte[] bArray = new byte[16];
        return null;
    }

}
