package fr.upem.net.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.*;

/**
 * Created by bemmanuel on 02/02/14.
 */
//Ce serveur est implémenté en respectant la RFC décrite
// dans le fichier RFC.md se trouvant à la racine de ce projet

/**
 * this class is create according to the RFC specify
 * on file RFC7777.pdf
 */
public class LongSumServerRFC {
    //all key of that map are session ID
    private final Map<Long, SocketAddress> adressMap;
    private final Map<Long, List<Long>> controlMap;
    private final Map<Long, Long> resultMap;

    private final DatagramSocket datagramSocket;


    public LongSumServerRFC(int port) throws SocketException {
        this.datagramSocket = new DatagramSocket(port);
        adressMap = new HashMap<>();
        controlMap = new HashMap<>();
        resultMap = new HashMap<>();
    }

    /**
     * this will start the server
     *
     * @throws IOException
     */
    public void launched() throws IOException {
        byte[] buf = new byte[25];

        DatagramPacket packet = new DatagramPacket(buf, buf.length);
        Long l = 0l;
        Long sessionId = null;
        Long dataId = null;
        while (true) {
            datagramSocket.receive(packet);
            sessionId = retreiveSessionId(buf);
            dataId = retreiveDataId(buf);
            l = retreiveLong(buf);
            SocketAddress socketAddress = packet.getSocketAddress();

            if (!adressMap.containsKey(sessionId)) {
                adressMap.put(sessionId, socketAddress);
                controlMap.put(sessionId, new ArrayList<Long>());
                controlMap.get(sessionId).add(dataId);
                resultMap.put(sessionId, l);
                continue;


            } else if (l != 0) {

                List<Long> dataIdList = controlMap.get(sessionId);
                //addition is done only if
                //the dataId haven't been register yet
                if (!dataIdList.contains(dataId)) {
                    Long aLong = resultMap.get(sessionId);
                    aLong += l;
                    resultMap.put(sessionId, aLong);
                }
            }

            List<Long> listOfNonReceiveFrame = createListOfNonReceiveFrame(sessionId);
            if (listOfNonReceiveFrame.isEmpty()) {
                Long somme = resultMap.get(sessionId);
                constructResultFrame(somme, buf);


            } else {
                for (Long dataIdList : listOfNonReceiveFrame) {
                    constructErrorFrame(dataIdList, buf);


                }
            }
            datagramSocket.send(packet);
            packet.setData(buf, 0, buf.length);


        }
    }

    /**
     * this is use to create an missing dataId frame
     *
     * @param dataId : the missing data Id
     * @param buf
     */
    private void constructErrorFrame(Long dataId, byte[] buf) {
        byte[] src = new byte[8];
        Converter.longToByteArray(dataId, src);
        System.arraycopy(src, 0, buf, 8, 8);
        buf[24] = 0;
    }


    /**
     * this will create the frame which contains the result of calculation
     *
     * @param somme
     * @param buf
     */
    private void constructResultFrame(Long somme, byte[] buf) {
        byte[] src = new byte[8];
        Converter.longToByteArray(somme, src);
        System.arraycopy(src, 0, buf, 16, 8);
        buf[24] = 1;

    }

    /**
     * this method will get all the missing dataId of the given sessionId
     * and return it as a list
     *
     * @param sessionId
     * @return
     */
    private List<Long> createListOfNonReceiveFrame(Long sessionId) {
        List<Long> dataIdList = controlMap.get(sessionId);
        Collections.sort(dataIdList);
        List<Long> missingDataId = new ArrayList<>();

        Long max = dataIdList.get(dataIdList.size() - 1);

        if (dataIdList.size() == max) {
            return missingDataId;
        } else {
            for (long l = 0; l < max; l++) {
                if (!dataIdList.contains(l)) {
                    missingDataId.add(l);
                }
            }
        }
        return missingDataId;
    }

    /**
     * parse the session id from the given buffer
     *
     * @param buf buffer to parse
     * @return
     */
    private Long retreiveSessionId(byte[] buf) {
        byte[] dest = new byte[8];
        System.arraycopy(buf, 0, dest, 0, 8);
        return Converter.byteArrayToLong(dest);
    }

    /**
     * parse the dataId id from the given buffer
     *
     * @param buf buffer to parse
     * @return
     */
    private Long retreiveDataId(byte[] buf) {
        byte[] dest = new byte[8];
        System.arraycopy(buf, 8, dest, 0, 8);
        return Converter.byteArrayToLong(dest);
    }

    /**
     * parse the long to addition from the given buffer
     *
     * @param buf buffer to parse
     * @return
     */
    private Long retreiveLong(byte[] buf) {
        byte[] dest = new byte[8];
        System.arraycopy(buf, 16, dest, 0, 8);
        return Converter.byteArrayToLong(dest);
    }

    public static void main(String[] args) throws IOException {
        LongSumServerRFC longSumServerRFC = new LongSumServerRFC(new Integer(args[0]));
        System.out.println("serveur launched");
        longSumServerRFC.launched();

    }


}
