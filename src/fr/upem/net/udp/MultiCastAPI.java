package fr.upem.net.udp;

import java.io.IOException;
import java.net.*;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: ebabalac
 * Date: 29/01/14
 * Time: 11:36
 * To change this template use File | Settings | File Templates.
 */
public class MultiCastAPI {

    //Pour savoir quelle appli a envoyer un message
    //on peut rajouter recupérer sa socketadress de l'appli
    // ainsi on est sur de toujours les distinguer


    public static void main(String[] args) throws IOException {


        String serveur = "239.252.0.117";
        int port = 7777;


        SocketAddress dest = new InetSocketAddress(serveur, port);
        byte[] buff = new byte[512];


        MulticastSocket multicastSocket = new MulticastSocket(port);
        multicastSocket.joinGroup(InetAddress.getByName(serveur));

        // Afin de fermer proprement cette Thread
        // Il suffit de declarer le writer en Daemon
        // Ainsi c'est la jvm qui va gerer la fermeture

        Thread writer = new Thread(() -> {

            while (true) {

                byte[] buf = new byte[512];
                DatagramPacket packet = new DatagramPacket(buf, buf.length, dest);
                try {
                    System.out.println("enter message to read");
                    Scanner scanner = new Scanner(System.in);
                    String next = scanner.nextLine();

                    buf = next.getBytes("UTF-8");

                    packet.setData(buf);
                    multicastSocket.send(packet);

                } catch (IOException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }

        });

        writer.setDaemon(true);
        Thread reader = new Thread(() -> {

            byte[] buf = new byte[512];
            DatagramPacket packet = new DatagramPacket(buf, buf.length, dest);
            while (true) {


                try {
                    System.out.println("wait for packet");
                    multicastSocket.receive(packet);
                    String s = new String(buf, 0, packet.getLength(), "UTF-8");
                    System.out.println("==>  " + s + "  received from" + packet.getSocketAddress());
                    //On arrete la thread si on recoit bye ou
                    //si cette thread a été interrompu précédemment
                    if (s.equals("BYE") || Thread.currentThread().isInterrupted()) {
                        Thread.currentThread().interrupt();
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }


        });
        reader.start();
        writer.start();
    }

}
