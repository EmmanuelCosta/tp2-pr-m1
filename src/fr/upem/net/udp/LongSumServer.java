package fr.upem.net.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ebabalac
 * Date: 29/01/14
 * Time: 08:59
 * To change this template use File | Settings | File Templates.
 */
public class LongSumServer {
    private final DatagramSocket datagramSocket;
    //this list was used for exo 1 only
    private final List<Long> longList = new ArrayList<>();
    private final Map<SocketAddress, Long> hashMap = new HashMap<>();

    public LongSumServer(int port) throws SocketException {

        this.datagramSocket = new DatagramSocket(port);

    }

    //EXO 1
    public void launch() throws IOException {
        byte[] buf = new byte[8];
        DatagramPacket packet = new DatagramPacket(buf, buf.length);
        try {
            while (true) {

                datagramSocket.receive(packet);
                Long l = Converter.byteArrayToLong(buf);
                System.out.println(l);


                longList.add(l);
                if (l == 0) {
                    Long somme = doSomme();
                    Converter.longToByteArray(somme, buf);

                    datagramSocket.send(packet);    //envoie le packet reponse
                    packet.setData(buf, 0, buf.length);   //Remet la taille à jour

                }


            }
        } finally {
            datagramSocket.close();
        }
    }

    public void launch2() throws IOException {
        byte[] buf = new byte[8];
        DatagramPacket packet = new DatagramPacket(buf, buf.length);
        Long l = 0l;
        try {
            while (true) {

                datagramSocket.receive(packet);
                l = Converter.byteArrayToLong(buf);
                System.out.println(l);
                SocketAddress socketAddress = packet.getSocketAddress();
                System.out.println(socketAddress);
                if (!hashMap.containsKey(socketAddress)) {


                    hashMap.put(socketAddress, l);

                } else {
                    Long aLong = hashMap.get(socketAddress);
                    aLong += l;
                    hashMap.put(socketAddress, aLong);
                }


                if (l == 0) {
                    Long somme = hashMap.get(socketAddress);
                    Converter.longToByteArray(somme, buf);

                    datagramSocket.send(packet);    //envoie le packet reponse
                    packet.setData(buf, 0, buf.length);   //Remet la taille à jour

                }


            }
        } finally {
            datagramSocket.close();
        }
    }

    /**
     * pour exo 1
     *
     * @return
     */
    private Long doSomme() {
        long soe = 0;
        for (Long l : longList) {
            soe += l;
        }
        longList.clear();
        return soe;
    }


    //Taille maximale pour question d'efficacité
    public static void main(String[] args) throws IOException {
        LongSumServer echoUDPServer = new LongSumServer(new Integer(args[0]));
        System.out.println("serveur launched");
        //exo 1
        //echoUDPServer.launch();
        //exo 2
        echoUDPServer.launch2();

    }
}
