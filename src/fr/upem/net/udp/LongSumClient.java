package fr.upem.net.udp;

import java.io.IOException;
import java.net.*;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: ebabalac
 * Date: 29/01/14
 * Time: 08:47
 * To change this template use File | Settings | File Templates.
 */
public class LongSumClient {
    public static void main(String[] args) throws IOException {
        if (args.length < 2) {
            System.err.println("Not enough arguments");
            return;
        }
        DatagramSocket socket = new DatagramSocket();
        //ENVOIE
        String serveur = args[0];
        String port = args[1];

        SocketAddress dest = new InetSocketAddress(serveur, new Integer(port));
        byte[] buf = new byte[8];
        DatagramPacket packet = new DatagramPacket(buf, buf.length, dest);


        byte[] buff = new byte[1024];
        while (true) {
            System.out.println("veuillez entrer les entiers long à sommer encoder");
            Scanner sc = new Scanner(System.in);
            Long l = sc.nextLong();


            Converter.longToByteArray(l, buf);


            socket.send(packet);
            if (l == 0) {
                break;
            }
        }

        //RECEPTION
        socket.setSoTimeout(2000);
        packet.setData(buff);
        try {
            socket.receive(packet);

            Long s = Converter.byteArrayToLong(buff);

            System.out.println("somme recue = " + s);


        } catch (SocketTimeoutException e) {
            System.out.println("Le serveur n'a pas repondu");
        } finally {
            socket.close();
        }
    }

}

